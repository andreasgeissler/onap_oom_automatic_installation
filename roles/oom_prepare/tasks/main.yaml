---
- name: configure os cloud
  import_tasks: clouds.yaml

- name: remove oom directory
  file:
    path: "{{ oom_path }}"
    state: absent

- name: set review_path (oom case)
  set_fact:
    review_path: "{{ oom_path }}"
  when: project == 'oom'

- name: "clone oom {{ branch }}"
  git:
    repo: "{{ oom_url }}"
    dest: "{{ oom_path }}"
    version: "{{ branch }}"
  when: not offline_mode

- name: "configure git"  # noqa 303
  shell: |
    git config --global user.email "You@example.com";
    git config --global user.name "Your Name"
  changed_when: "false"

- name: "clone oom {{ branch }} without direct connectivity"
  block:
    - name: create oom directory
      file:
        path: "{{ oom_path }}"
        state: directory

    - name: "clone oom {{ branch }}"
      git:
        repo: "{{ oom_url }}"
        dest: /tmp/oom
        version: "{{ branch }}"
        depth: 1
      delegate_to: 127.0.0.1

    - name: archive oom sources
      archive:
        path: /tmp/oom/*
        dest: /tmp/oom.zip
        format: zip
      delegate_to: 127.0.0.1

    - name: extract oom sources
      unarchive:
        src: /tmp/oom.zip
        dest: "{{ oom_path }}"

    - name: copy patch oom for mvp
      template:
        src: oom_patch.txt.j2
        dest: /tmp/oom_patch.txt
        variable_start_string: "[{[{"
        variable_end_string: "}]}]"

    - name: patch oom for offline
      command: git apply /tmp/oom_patch.txt
      args:
        chdir: "{{ oom_path }}"
      tags:
        - skip_ansible_lint
  when: offline_mode

- name: override helm path for CoreOS
  set_fact:
    helm_path: /home/core/bin/helm
  when: ansible_os_family | lower == "coreos"

- name: retrieve review_path and clone when not in oom case
  block:
    - name: set review_path (not oom case)
      set_fact:
        review_path: "{{ oom_path }}/{{ project_dir_mapping[project] }}"

    - name: ensure review directory is not there
      file:
        path: "{{ review_path }}"
        state: absent

    - name: "clone {{ project }} {{ branch }}"
      git:
        repo: "{{ onap_base_url }}/{{ project }}"
        dest: "{{ review_path }}"
        version: "{{ branch }}"
  when: project != 'oom' and 'oom' in project

- name: generate review end of url
  set_fact:
    review_end_url:
      "{{ gerrit_review[-2:] }}/{{ gerrit_review }}/{{ gerrit_patchset }}"
  when: gerrit_review and 'oom' in project

- name: "retrieve change branch for project {{ project }}"  # noqa 303
  shell:
    cmd: >
      git pull --no-edit {{ onap_base_url }}/{{ project }}
      refs/changes/{{ review_end_url }}
    chdir: "{{ review_path }}"
  when: gerrit_review and 'oom' in project


- name: check if helm has been initialized
  stat:
    path: "{{ ansible_user_dir }}/.helm"
  register: helm_directory

- name: init helm
  command: helm init
  when: not helm_directory.stat.exists

- name: copy oom help plugins
  synchronize:
    src: "{{ oom_path }}/kubernetes/helm/plugins"
    dest: "{{ ansible_user_dir }}/.helm/"
  delegate_to: "{{ inventory_hostname }}"

- name: start helm server
  become: "yes"
  command:
    "start-stop-daemon --start --background --oknodo \
     --exec /usr/local/bin/helm -- serve --home {{ ansible_user_dir }}/.helm"
  changed_when: "true"
  when: ansible_os_family | lower != "coreos"

- name: add helm local repository
  command: "helm repo add local http://127.0.0.1:{{ helm_server_port }}"
  changed_when: "true"

- name: compile helm packages
  command: "make all"
  changed_when: "true"
  args:
    chdir: "{{ oom_path }}/kubernetes"
  environment:
    SKIP_LINT: "{{ gerrit_review | ternary('TRUE', 'FALSE') }}"


- name: create onap namespace
  run_once: "yes"
  k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Namespace
      metadata:
        name: onap
  when: add_v2_identity_wrapper|bool

- name: add v3 at end of os_auth_url
  set_fact:
    os_auth_url:
      "{{ ((os_auth_url[-3:] == 'v3/') or (os_auth_url[-2:] == 'v3')) |
        ternary(os_auth_url | regex_replace('/$', ''),
          (os_auth_url[-1:] == '/') | ternary(
            os_auth_url ~ 'v3',
            os_auth_url ~ '/v3')) }}"

- name: create identity wrapper deployment
  run_once: "yes"
  k8s:
    state: present
    definition:
      apiVersion: extensions/v1beta1
      kind: Deployment
      metadata:
        name: identity-wrapper
        namespace: onap
      spec:
        replicas: 1
        template:
          metadata:
            labels:
              app: identity-wrapper
          spec:
            containers:
              - name: identity-wrapper
                image: sdesbure/identity_wrapper:latest
                imagePullPolicy: Always
                env:
                  - name: KEYSTONE_URL
                    value: "{{ os_auth_url }}"
                  - name: USER_DOMAIN_NAME
                    value: "{{ os_user_domain_name }}"
                ports:
                  - containerPort: 5000
  when: add_v2_identity_wrapper|bool

- name: create identity wrapper service
  run_once: "yes"
  k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: identity-wrapper
        namespace: onap
      spec:
        ports:
          - name: http
            port: 5000
            protocol: TCP
            targetPort: 5000
        selector:
          app: identity-wrapper
        type: LoadBalancer
  when: add_v2_identity_wrapper|bool
